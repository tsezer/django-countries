import os
from setuptools import setup, find_packages

packages = find_packages()

setup(
    name='django-countries',
    version='0.0.0',
    author='Fredrik Sjoblom',
    author_email='',
    packages=packages,
    install_requires = [],
    include_package_data = True,
    exclude_package_data={
        '': ['*.sql', '*.pyc',],
    },
    url='http://code.google.com/p/django-countries/',
    license='LICENSE.txt',
    description='Table of Countries.',
    long_description=open('README.txt').read(),
)
